package org.astropsynapse.Amethyst.events;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class eventAsyncPlayerChat implements Listener {
    private final Amethyst plugin;
    public eventAsyncPlayerChat(Amethyst instance) { plugin = instance; }

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        Player p = event.getPlayer();
        String uuid = p.getUniqueId().toString();
        if (plugin.config.getBoolean(String.format("players.%s.muted", uuid))) {
            p.sendMessage(ChatColor.RED + "You are muted and can not chat.");
            event.setCancelled(true);
        }
    }
}