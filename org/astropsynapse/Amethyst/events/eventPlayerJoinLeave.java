package org.astropsynapse.Amethyst.events;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class eventPlayerJoinLeave implements Listener {
    private final Amethyst plugin;
    public eventPlayerJoinLeave(Amethyst instance) { plugin = instance; }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        String uuid = p.getUniqueId().toString();
        if (plugin.config.getBoolean("showRealmJoinLeaveMessages")) {
            String username = plugin.config.getString(String.format("players.%s.nickname", uuid));
            if (username == null)
                username = p.getName();
            event.setJoinMessage(String.format("%s%s%s joins the realm", ChatColor.GRAY, ChatColor.ITALIC, username));
            p.setDisplayName(username);
            p.setPlayerListName(username);
        }
        if (plugin.config.getBoolean("showMotdOnJoin")) {
            p.performCommand("motd");
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        if (plugin.config.getBoolean("showRealmJoinLeaveMessages")) {
            event.setQuitMessage(String.format("%s%s%s left the realm", ChatColor.GRAY, ChatColor.ITALIC, p.getName()));
        }
    }
}
