package org.astropsynapse.Amethyst.events;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

@SuppressWarnings("unused")
public class eventPlayerDeath implements Listener {
    private final Amethyst plugin;

    public eventPlayerDeath(Amethyst instance) {
        plugin = instance;
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        String uuid = player.getUniqueId().toString();
        plugin.config.set(String.format("players.%s.lastlocation.world", uuid), player.getWorld().getName());
        plugin.config.set(String.format("players.%s.lastlocation.x", uuid), player.getLocation().getX());
        plugin.config.set(String.format("players.%s.lastlocation.y", uuid), player.getLocation().getY());
        plugin.config.set(String.format("players.%s.lastlocation.z", uuid), player.getLocation().getZ());
        plugin.config.set(String.format("players.%s.lastlocation.pitch", uuid), player.getLocation().getPitch());
        plugin.config.set(String.format("players.%s.lastlocation.yaw", uuid), player.getLocation().getYaw());
        plugin.saveConfig();
    }
}
