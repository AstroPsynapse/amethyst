package org.astropsynapse.Amethyst.events;
import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

public class eventPlayerTeleport implements Listener {
    private final Amethyst plugin;
    public eventPlayerTeleport(Amethyst instance) {
        plugin = instance;
    }
    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (event.getCause().toString().equals("UNKNOWN")) // Disregard UNKNOWN teleport event
            return;
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        if (plugin.config.getBoolean("debug"))
            player.sendMessage(String.format("[Debug] Teleport event raised. Cause: %s", event.getCause().toString()));
        plugin.config.set(String.format("players.%s.lastlocation.world", uuid), event.getFrom().getWorld().getName());
        plugin.config.set(String.format("players.%s.lastlocation.x", uuid), event.getFrom().getX());
        plugin.config.set(String.format("players.%s.lastlocation.y", uuid), event.getFrom().getY());
        plugin.config.set(String.format("players.%s.lastlocation.z", uuid), event.getFrom().getZ());
        plugin.config.set(String.format("players.%s.lastlocation.pitch", uuid), event.getFrom().getPitch());
        plugin.config.set(String.format("players.%s.lastlocation.yaw", uuid), event.getFrom().getYaw());
        plugin.saveConfig();
    }
}