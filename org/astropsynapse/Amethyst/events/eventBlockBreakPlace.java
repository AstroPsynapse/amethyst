package org.astropsynapse.Amethyst.events;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class eventBlockBreakPlace implements Listener {
    private final Amethyst plugin;
    public eventBlockBreakPlace(Amethyst instance) { plugin = instance; }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player p = event.getPlayer();
        if (!plugin.config.getBoolean("buildingAllowed") && !p.hasPermission("luassentials.protection.build")) {
            p.sendMessage(String.format("%sYou are not allowed to break blocks.", ChatColor.RED));
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player p = event.getPlayer();
        if (!plugin.config.getBoolean("buildingAllowed") && !p.hasPermission("luassentials.protection.build")) {
            p.sendMessage(String.format("%sYou are not allowed to place blocks.", ChatColor.RED));
            event.setCancelled(true);
        }
    }
}
