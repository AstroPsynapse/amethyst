package org.astropsynapse.Amethyst.events;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class eventWeatherChange implements Listener {
    private final Amethyst plugin;

    public eventWeatherChange(Amethyst instance) { plugin = instance; }


    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (plugin.config.getBoolean("announceWeatherChanges")) {
            if (event.toWeatherState())
                plugin.getServer().broadcastMessage(String.format("%s%sIt begins to rain...", ChatColor.GRAY, ChatColor.ITALIC));
            else
                plugin.getServer().broadcastMessage(String.format("%s%sThe sky begins to clear...", ChatColor.GRAY, ChatColor.ITALIC));
        }
    }
}
