package org.astropsynapse.Amethyst.commands;
import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandMute implements CommandExecutor {
    private final Amethyst plugin;
    public commandMute(Amethyst instance) { plugin = instance; }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("amethyst.moderation.mute")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else if (args.length < 1) {
            return false;
        }
        else {
            Player p = plugin.getServer().getPlayer(args[0]);
            if (p == null) {
                sender.sendMessage(ChatColor.RED + "Error - The specified player could not be found.");
                return true;
            }
            String uuid = p.getUniqueId().toString();
            boolean muted = plugin.config.getBoolean(String.format("players.%s.muted", uuid));
            if (!muted) {
                plugin.config.set(String.format("players.%s.muted", uuid), true);
                p.sendMessage(ChatColor.RED + "You have been muted by a moderator.");
            }
            else {
                plugin.config.set(String.format("players.%s.muted", uuid), false);
                p.sendMessage(ChatColor.RED + "You were unmuted by a moderator.");
            }
            plugin.saveConfig();
            return true;
        }
    }
}
