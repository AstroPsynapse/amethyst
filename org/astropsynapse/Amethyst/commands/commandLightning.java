package org.astropsynapse.Amethyst.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandLightning implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.moderation.lightning")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            Player p = (Player)sender;
            Block block = p.getTargetBlock(null, 100);
            Location location = block.getLocation();
            p.getWorld().strikeLightning(location);
            return true;
        }
    }
}