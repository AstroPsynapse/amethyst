package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class commandMotd implements CommandExecutor {
    private final Amethyst plugin;
    public commandMotd(Amethyst instance) { plugin = instance; }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("amethyst.user.motd")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            String line;
            try {
                FileReader fileReader = new FileReader(plugin.getDataFolder().getPath() + "/motd.txt");
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                while((line = bufferedReader.readLine()) != null) {
                    if (line.length() < 1) continue;
                    if (line.length() >= 2)
                        if (line.substring(0, 2).equals("##"))
                            continue;
                    sender.sendMessage(plugin.motdFormat(line));
                }
                bufferedReader.close();
            }
            catch(FileNotFoundException ex) {
                sender.sendMessage(ChatColor.RED + "Error - There was no motd.txt found.");
            }
            catch(IOException ex) {
                sender.sendMessage(ChatColor.RED + "Error - The motd.txt file could not be read.");
            }
        }
        return true;
    }
}
