package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;




public class commandAmethyst implements CommandExecutor {
    private final Amethyst plugin;

    public commandAmethyst(Amethyst instance) { plugin = instance; }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("amethyst.admin")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            if (args.length < 1) {
                sender.sendMessage(String.format("Amethyst %s - Administration", plugin.getDescription().getVersion()));
                sender.sendMessage("Commands: reload, config");
                return true;
            }
            else {

                switch (args[0].toLowerCase()) {
                    case "reload":
                        plugin.reloadConfig();
                        sender.sendMessage(String.format("Amethyst %s configuration file has been reloaded.", plugin.getDescription().getVersion()));
                        return true;
                    case "config":
                        String[] configurationNodes = {"maxHomes, showMotdOnJoin, showRealmJoinLeaveMessages, buildingAllowed, announceWeatherChanges"};
                        if (args.length > 1) {
                            String configNode;
                            switch (args[1].toLowerCase()) {
                                case "maxhomes":
                                    configNode = "maxHomes";
                                    break;
                                case "showmotdonjoin":
                                    configNode = "showMotdOnJoin";
                                    break;
                                case "showrealmjoinleavemessages":
                                    configNode = "showRealmJoinLeaveMessages";
                                    break;
                                case "buildingallowed":
                                    configNode = "buildingAllowed";
                                    break;
                                case "announceweatherchanges":
                                    configNode = "announceWeatherChanges";
                                    break;
                                default:
                                    sender.sendMessage("Available configuration options:");
                                    sender.sendMessage(String.join(", ", configurationNodes));
                                    return true;
                            }
                                if (args.length > 2) {
                                    boolean isTrueFalse = true;
                                    if (configNode.equalsIgnoreCase("maxhomes"))
                                        isTrueFalse = false;

                                    if (!isTrueFalse) {
                                        try {
                                            plugin.config.set(configNode, Integer.parseInt(args[2]));
                                            plugin.saveConfig();
                                            sender.sendMessage(String.format("'%s' has been updated to %s", configNode, args[2]));
                                        } catch (NumberFormatException ex) {
                                            sender.sendMessage(ChatColor.RED + "Error - Please enter a valid number.");
                                            return true;
                                        }
                                    }
                                    else {
                                        switch(args[2].toLowerCase()) {
                                            case "true":
                                                plugin.config.set(configNode, true);
                                                break;
                                            case "false":
                                                plugin.config.set(configNode, false);
                                                break;
                                            default:
                                                sender.sendMessage(ChatColor.RED + "Error - Please enter true or false.");
                                                return true;
                                        }
                                        plugin.saveConfig();
                                        sender.sendMessage(String.format("'%s' has been updated to %s", configNode, args[2]).toLowerCase());
                                        return true;
                                    }
                                }
                                else {
                                    sender.sendMessage(ChatColor.RED + "Error - Invalid option for configuration node.");
                                    return true;
                                }
                        }
                        else {
                            sender.sendMessage("Available configuration options:");
                            sender.sendMessage("maxHomes, showMotdOnJoin, showRealmJoinLeaveMessages, buildingAllowed, announceWeatherChanges");
                            return true;
                        }
                        break;
                }
                return true;
            }
        }
    }
}


