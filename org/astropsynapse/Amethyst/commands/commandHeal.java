package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class commandHeal implements CommandExecutor {
    private final Amethyst plugin;
    public commandHeal(Amethyst instance) { plugin = instance; }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("amethyst.moderation.heal")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            Player target;

            if (args.length == 0) {
                if (sender instanceof ConsoleCommandSender) {
                    sender.sendMessage(ChatColor.RED + "Error - No player was specified.");
                    return true;
                }
                target = (Player)sender;
            }
            else {
                if (!sender.hasPermission("amethyst.moderation.heal.other")) {
                    sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
                    return true;
                }
                target = plugin.getServer().getPlayer(args[0]);
            }
            if (target == null) {
                sender.sendMessage(ChatColor.RED + "Error - Could not find the specified player.");
                return true;
            }
            target.setHealth(20.0);
            target.setFoodLevel(20);
            target.sendMessage(ChatColor.GREEN + "Health and hunger restored!");
            if (target != sender) {
                sender.sendMessage(ChatColor.GOLD + String.format("You healed %s", target.getName()));
            }
            return true;
        }
    }
}
