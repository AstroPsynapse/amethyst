package org.astropsynapse.Amethyst.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandSetspawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.moderation.setspawn")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            Player p = (Player)sender;
            int x = (int)p.getLocation().getX(),
                y = (int)p.getLocation().getY(),
                z = (int)p.getLocation().getZ();
            p.getWorld().setSpawnLocation(x,y,z);
            p.sendMessage(String.format("This world's spawn location is now [X: %d, Y: %d, Z: %d]", x, y, z));
        }
        return true;
    }
}
