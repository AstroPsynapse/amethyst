package org.astropsynapse.Amethyst.commands.completers;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class commandGiveCompleter implements TabCompleter {
    private final Amethyst plugin;

    public commandGiveCompleter(Amethyst instance) { plugin = instance; }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        if (args.length == 1) {
            List<String> list = new ArrayList<>();
            for (Player player : plugin.getServer().getOnlinePlayers()) {
                if (player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    list.add(player.getName());
            }
            return list;
        }
        else if (args.length == 2) {
            List<String> list = new ArrayList<>();
            for (String mat : plugin.staticLists.Materials) {
                if (mat.toLowerCase().startsWith(args[1].toLowerCase()))
                    list.add(mat);
            }
            return list;
        }
        return null;
    }
}
