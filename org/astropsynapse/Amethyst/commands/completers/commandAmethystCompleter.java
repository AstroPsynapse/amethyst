package org.astropsynapse.Amethyst.commands.completers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import java.util.ArrayList;
import java.util.List;

public class commandAmethystCompleter implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
        final String[] subCommands = {"config", "reload"};
        final String[] configNodes = {"maxHomes", "showMotdOnJoin", "showRealmJoinLeaveMessages", "buildingAllowed", "announceWeatherChanges"};
        List<String> list = new ArrayList<>();
        if (args.length == 1) {
            for (String subCommand : subCommands) {
                if (subCommand.toLowerCase().startsWith(args[0].toLowerCase()))
                    list.add(subCommand);
            }
            return list;
        }
        else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("config")) {
                for (String configNode : configNodes) {
                    if (configNode.toLowerCase().startsWith(args[1].toLowerCase()))
                        list.add(configNode);
                }
                return list;
            }
        }
        else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("config") && !args[1].equalsIgnoreCase("maxHomes")) {
                String[] configSelections = {"true", "false"};
                for (String configSelection : configSelections) {
                    if (configSelection.toLowerCase().startsWith(args[2].toLowerCase()))
                        list.add(configSelection);
                }
                return list;
            }
        }
        return null;
    }
}