package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandTp implements CommandExecutor {
    private final Amethyst plugin;

    public commandTp(Amethyst instance) { plugin = instance; }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.teleport.tp")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            if (args.length == 1) { // tp
                Player p = plugin.getServer().getPlayer(args[0]);
                if (p == null) {
                    sender.sendMessage(ChatColor.RED + "Error - We could not find the specified player.");
                    return true;
                }
                ((Player) sender).teleport(p.getLocation());
                sender.sendMessage(String.format("Teleported to %s", p.getName()));
                return true;

            } else if (args.length >= 2) { // tp.other
                if (!sender.hasPermission("amethyst.teleport.tp.other")) {
                    sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
                    return true;
                }
                Player p1 = plugin.getServer().getPlayer(args[0]);
                Player p2 = plugin.getServer().getPlayer(args[1]);
                if (p1 == null) {
                    sender.sendMessage(String.format(ChatColor.RED + "Error - We could not find a player name containing: %s", args[0]));
                    return true;
                }
                if (p2 == null) {
                    sender.sendMessage(String.format(ChatColor.RED + "Error - We could not find a player name containing: %s", args[1]));
                    return true;
                }
                p1.teleport(p2.getLocation());
                sender.sendMessage(String.format("You teleported %s to %s", p1.getName(), p2.getName()));
                p1.sendMessage(String.format("You were teleported to %s by %s", p2.getName(), sender.getName()));
                return true;
            }
        }
        return false;
    }
}
