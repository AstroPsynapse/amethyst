package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandDelhome implements CommandExecutor {
    private final Amethyst plugin;
    public commandDelhome(Amethyst instance) {
        plugin = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.home.delhome")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else if (args.length < 1) {
            return false;
        }
        else {
            Player p = (Player)sender;
            String uuid = p.getUniqueId().toString();
            int maxHomes = plugin.config.getInt("maxHomes");
            if (maxHomes <= 0) {
                sender.sendMessage("Homes are currently disabled.");
                return true;
            }
            for (int i = 1; i <= maxHomes; i++) {
                String homeName = plugin.config.getString(String.format("players.%s.homes.%s.name", uuid, i));
                if (homeName == null) continue;
                if (homeName.equalsIgnoreCase(args[0])) {
                    plugin.config.set(String.format("players.%s.homes.%s", uuid, i), null);
                    sender.sendMessage(String.format("'%s' has been removed from your homes list.", args[0]));
                    plugin.saveConfig();
                    return true;
                }
            }
            sender.sendMessage(String.format("'%s' was not found in your saved homes list.", args[0]));
            return true;

        }
    }
}
