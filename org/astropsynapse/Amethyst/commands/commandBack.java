package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandBack implements CommandExecutor {
    private final Amethyst plugin;
    public commandBack(Amethyst instance) {
        plugin = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.teleport.back")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            Player p = (Player)sender;
            String uuid = p.getUniqueId().toString();
            String worldName = plugin.config.getString(String.format("players.%s.lastlocation.world", uuid));
            if (worldName == null) {
                sender.sendMessage(ChatColor.RED + "Error - No previous location was found.");
                return true;
            }
            float x = (float)plugin.config.getDouble(String.format("players.%s.lastlocation.x", uuid));
            float y = (float)plugin.config.getDouble(String.format("players.%s.lastlocation.y", uuid));
            float z = (float)plugin.config.getDouble(String.format("players.%s.lastlocation.z", uuid));
            float pitch = (float)plugin.config.getDouble(String.format("players.%s.lastlocation.pitch", uuid));
            float yaw = (float)plugin.config.getDouble(String.format("players.%s.lastlocation.yaw", uuid));
            World world = plugin.getServer().getWorld(worldName);
            Location location = new Location(world, x, y, z, yaw, pitch);
            p.teleport(location);
            p.sendMessage("You were teleported to your previous location.");
        }
        return true;
    }
}