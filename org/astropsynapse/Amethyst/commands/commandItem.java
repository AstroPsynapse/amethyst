package org.astropsynapse.Amethyst.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class commandItem implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.moderation.item")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            Player p = (Player)sender;
            if (args.length > 0) {
                Material material = Material.getMaterial(args[0].toUpperCase());
                if (material == null) {
                    p.sendMessage(ChatColor.RED + "Error - That item does not exist.");
                    return true;
                }

                int amount = 1;
                if (args.length > 1) {
                    try {
                        amount = Integer.parseInt(args[1]);
                    }
                    catch (NumberFormatException ex) {
                        p.sendMessage(ChatColor.RED + "Error - Please enter a valid amount.");
                        return true;
                    }
                }
                ItemStack itemStack = new ItemStack(material, amount);
                p.getInventory().addItem(itemStack);
                p.sendMessage(String.format("Added %d %s to your inventory.", amount, itemStack.getType().toString().replace("_", " ").toLowerCase()));
                return true;
            }
            else {
                return false;
            }
        }
    }
}
