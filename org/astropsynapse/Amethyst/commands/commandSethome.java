package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandSethome implements CommandExecutor {
    private final Amethyst plugin;
    public commandSethome(Amethyst instance) {
        plugin = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.home.sethome")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else if (args.length < 1) {
            return false;
        }
        else {
            Player p = (Player)sender;
            String uuid = p.getUniqueId().toString();
            int maxHomes = plugin.config.getInt("maxHomes");
            if (maxHomes <= 0) {
                sender.sendMessage("Homes are currently disabled.");
                return true;
            }
            for (int i = 1; i <= maxHomes; i++) {
                String homeName = plugin.config.getString(String.format("players.%s.homes.%s.name", uuid, i));
                if (homeName == null || homeName.equalsIgnoreCase(args[0])) {
                    plugin.config.set(String.format("players.%s.homes.%s.name", uuid, i), args[0]);
                    plugin.config.set(String.format("players.%s.homes.%s.world", uuid, i), p.getWorld().getName());
                    plugin.config.set(String.format("players.%s.homes.%s.x", uuid, i), p.getLocation().getX());
                    plugin.config.set(String.format("players.%s.homes.%s.y", uuid, i), p.getLocation().getY() + 0.25f);
                    plugin.config.set(String.format("players.%s.homes.%s.z", uuid, i), p.getLocation().getZ());
                    plugin.config.set(String.format("players.%s.homes.%s.pitch", uuid, i), p.getLocation().getPitch());
                    plugin.config.set(String.format("players.%s.homes.%s.yaw", uuid, i), p.getLocation().getYaw());
                    plugin.saveConfig();
                    p.sendMessage(String.format("'%s' has been saved to your homes list.", args[0]));
                    return true;
                }
            }
            p.sendMessage("You have reached the maximum number of homes you can save. Use /delhome to remove one.");
            return true;
        }
    }
}
