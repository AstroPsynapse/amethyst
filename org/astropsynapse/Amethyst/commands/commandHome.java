package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandHome implements CommandExecutor {
    private final Amethyst plugin;
    public commandHome(Amethyst instance) { plugin = instance; }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.home.home")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else if (args.length < 1) {
            ((Player) sender).performCommand("homes");
            return true;
        }
        else {
            Player p = (Player)sender;
            String uuid = p.getUniqueId().toString();
            int maxHomes = plugin.config.getInt("maxHomes");
            if (maxHomes <= 0) {
                sender.sendMessage("Homes have been disabled.");
                return true;
            }
            for (int i = 1; i <= maxHomes; i++) {
                String homeName = plugin.config.getString(String.format("players.%s.homes.%s.name", uuid, i));
                if (homeName == null) continue;
                if (!homeName.equalsIgnoreCase(args[0])) continue;
                String worldName = plugin.config.getString(String.format("players.%s.homes.%s.world", uuid, i));
                float x = (float)plugin.config.getDouble(String.format("players.%s.homes.%s.x", uuid, i));
                float y = (float)plugin.config.getDouble(String.format("players.%s.homes.%s.y", uuid, i));
                float z = (float)plugin.config.getDouble(String.format("players.%s.homes.%s.z", uuid, i));
                float yaw = (float)plugin.config.getDouble(String.format("players.%s.homes.%s.yaw", uuid, i));
                float pitch = (float)plugin.config.getDouble(String.format("players.%s.homes.%s.pitch", uuid, i));
                World world = plugin.getServer().getWorld(worldName);
                Location location = new Location(world, x, y, z, yaw, pitch);
                p.teleport(location);
                p.sendMessage(String.format("Teleported to your '%s' home.", homeName));
                return true;
            }
            p.sendMessage(String.format(ChatColor.RED + "Error - '%s' was not found in your list of saved homes.", args[0]));
        }
        return true;
    }
}
