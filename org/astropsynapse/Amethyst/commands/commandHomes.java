package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandHomes implements CommandExecutor {
    private final Amethyst plugin;
    public commandHomes(Amethyst instance) { plugin = instance; }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.home.home")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            Player player = (Player)sender;
            String uuid = player.getUniqueId().toString();
            int maxHomes = plugin.config.getInt("maxHomes");
            if (maxHomes <= 0) {
                player.sendMessage(ChatColor.RED + "Error - Homes have been disabled.");
                return true;
            }
            StringBuilder homeListBuilder = new StringBuilder();
            for (int i = 1; i <= maxHomes; i++) {
                String homeName = plugin.config.getString(String.format("players.%s.homes.%s.name", uuid, i));
                if (homeName == null) continue;

                if (homeListBuilder.toString().equals(""))
                    homeListBuilder = new StringBuilder(homeName);
                else
                    homeListBuilder.append(", ").append(homeName);
            }
            String homeList = homeListBuilder.toString();
            if (homeList.length() < 1)
                player.sendMessage(ChatColor.RED + "Error - No saved homes were found.");
            else
                player.sendMessage(String.format("Saved homes: %s", homeList));
        }
        return true;
    }
}
