package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandNick implements CommandExecutor{
    public final Amethyst plugin;
    public commandNick(Amethyst instance) { plugin = instance; }
    //TODO: Add in username length limit to configuration
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.user.nick")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else if (args.length < 1) {
            Player p = (Player)sender;
            String uuid = p.getUniqueId().toString();
            String nickname = plugin.config.getString(String.format("players.%s.nickname", uuid));
            if (nickname == null) {
                sender.sendMessage("You don't have a nickname currently set.");
            }
            else {
                String username = p.getName();
                plugin.config.set(String.format("players.%s.nickname", uuid), null);
                sender.sendMessage(String.format("Your nickname was reset to: %s", username));
                if (!username.equals(p.getDisplayName())) {
                    p.setDisplayName(username);
                    p.setPlayerListName(username);
                }
            }
            return true;
        }
        else {
            Player p = (Player)sender;
            String uuid = p.getUniqueId().toString();
            plugin.config.set(String.format("players.%s.nickname", uuid), args[0]);
            plugin.saveConfig();
            String nickname = plugin.config.getString(String.format("players.%s.nickname", uuid));
            p.setDisplayName(nickname);
            p.setPlayerListName(nickname);
            p.sendMessage(String.format("Your nickname has been set to: %s", args[0]));
            return true;
        }
    }
}
