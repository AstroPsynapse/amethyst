package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandSmite implements CommandExecutor {
    private final Amethyst plugin;
    public commandSmite(Amethyst instance) { plugin = instance; }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {



        if (!sender.hasPermission("amethyst.moderation.lightning")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            if (args.length < 1)
                return false;
            Player receiver = plugin.getServer().getPlayer(args[0]);
            if (receiver == null) {
                sender.sendMessage(ChatColor.RED + "Error - The specified player could not be found.");
                return true;
            }
            receiver.getWorld().strikeLightning(receiver.getLocation());
            receiver.sendMessage(ChatColor.GOLD + "Thou hast been smitten!");
            sender.sendMessage(String.format("Smited %s.", receiver.getName()));
            return true;
        }
    }
}
