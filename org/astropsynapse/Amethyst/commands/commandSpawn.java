package org.astropsynapse.Amethyst.commands;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandSpawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error - You can't use this command from the console.");
            return true;
        }
        else if (!sender.hasPermission("amethyst.teleport.spawn")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            Player player = (Player) sender;
            Location spawn = player.getWorld().getSpawnLocation();
            if (spawn.getX() == 0 && spawn.getY() == 0 && spawn.getZ() == 0) {
                player.sendMessage(ChatColor.RED + "Error - The world spawn location is invalid.");
                return true;
            }
            player.teleport(spawn);
            player.sendMessage("You were teleported to the world's spawn location.");
        }
        return true;
    }
}