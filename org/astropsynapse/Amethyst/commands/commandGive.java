package org.astropsynapse.Amethyst.commands;

import org.astropsynapse.Amethyst.Amethyst;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


public class commandGive implements CommandExecutor {
    private final Amethyst plugin;

    public commandGive(Amethyst instance) { plugin = instance; }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {


        if (!sender.hasPermission("amethyst.moderation.give")) {
            sender.sendMessage(ChatColor.RED + "Error - You do not have permission to use this command.");
            return true;
        }
        else {
            if (args.length > 0) {
                Player receiver = plugin.getServer().getPlayer(args[0]);
                if (receiver == null) {
                    sender.sendMessage(ChatColor.RED + "Error - That player could not be found.");
                    return true;
                }
                if (args.length > 1) {
                    Material material = Material.getMaterial(args[1].toUpperCase());
                    if (material == null) {
                        sender.sendMessage(ChatColor.RED + "Error - That item does not exist.");
                        return true;
                    }
                    int amount;
                    if (args.length > 2) {
                        try {
                            amount = Integer.parseInt(args[2]);
                        }
                        catch (NumberFormatException ex) {
                            sender.sendMessage(ChatColor.RED + "Error - Please enter a valid amount.");
                            return true;
                        }
                    }
                    else amount = 1;
                    ItemStack itemStack = new ItemStack(material, amount);
                    String itemName = itemStack.getType().toString().replace("_", " ").toLowerCase();
                    receiver.getInventory().addItem(itemStack);
                    receiver.sendMessage(String.format("You were given %d %s by %s", amount, itemName, sender.getName()));
                    receiver.sendMessage(String.format("You gave %d %s to %s", amount, itemName, receiver.getName()));
                    return true;
                }
                else return false;
            }
            else return false;
        }
    }
}