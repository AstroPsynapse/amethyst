package org.astropsynapse.Amethyst;

import org.astropsynapse.Amethyst.commands.*;
import org.astropsynapse.Amethyst.commands.completers.commandAmethystCompleter;
import org.astropsynapse.Amethyst.commands.completers.commandGiveCompleter;
import org.astropsynapse.Amethyst.commands.completers.commandItemCompleter;
import org.astropsynapse.Amethyst.events.*;
import org.astropsynapse.Amethyst.resources.StaticLists;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class Amethyst extends JavaPlugin {

    private final Logger log;
    public final FileConfiguration config;
    private final PluginManager pm;
    public final StaticLists staticLists;
    public final ConsoleCommandSender console;
    private final String version;
    public Amethyst() {
        log = getServer().getLogger();
        config = this.getConfig();
        pm = getServer().getPluginManager();
        staticLists = new StaticLists();
        console = this.getServer().getConsoleSender();
        version = this.getDescription().getVersion();
    }
    @Override
    public void onEnable() {
        // Global Variables

        // Event Listeners
        pm.registerEvents(new eventPlayerJoinLeave(this), this);
        pm.registerEvents(new eventPlayerTeleport(this), this);
        pm.registerEvents(new eventPlayerDeath(this), this);
        pm.registerEvents(new eventAsyncPlayerChat(this), this);
        pm.registerEvents(new eventWeatherChange(this), this);
        pm.registerEvents(new eventBlockBreakPlace(this), this);

        // Command Executors
        this.getCommand("spawn").setExecutor(new commandSpawn());
        this.getCommand("ci").setExecutor(new commandClearinventory());
        this.getCommand("back").setExecutor(new commandBack(this));
        this.getCommand("sethome").setExecutor(new commandSethome(this));
        this.getCommand("delhome").setExecutor(new commandDelhome(this));
        this.getCommand("home").setExecutor(new commandHome(this));
        this.getCommand("motd").setExecutor(new commandMotd(this));
        this.getCommand("tp").setExecutor(new commandTp(this));
        this.getCommand("homes").setExecutor(new commandHomes(this));
        this.getCommand("item").setExecutor(new commandItem());
        this.getCommand("item").setTabCompleter(new commandItemCompleter(this));
        this.getCommand("lightning").setExecutor(new commandLightning());
        this.getCommand("amethyst").setExecutor(new commandAmethyst(this));
        this.getCommand("amethyst").setTabCompleter(new commandAmethystCompleter());
        this.getCommand("give").setExecutor(new commandGive(this));
        this.getCommand("give").setTabCompleter(new commandGiveCompleter(this));
        this.getCommand("setspawn").setExecutor(new commandSetspawn());
        this.getCommand("mute").setExecutor(new commandMute(this));
        this.getCommand("smite").setExecutor(new commandSmite(this));
        this.getCommand("heal").setExecutor(new commandHeal(this));
        this.getCommand("nick").setExecutor(new commandNick(this));

        // Initialization Tasks
        loadConfig();
        console.sendMessage(ChatColor.GREEN + "Amethyst " + this.version + " is now enabled.");
    }

    @Override
    public void onDisable() { console.sendMessage(ChatColor.GOLD + "Amethyst " + this.version + " is now disabled."); }

    private void loadConfig() {
        config.addDefault("maxHomes", 3);
        config.addDefault("showMotdOnJoin", true);
        config.addDefault("showRealmJoinLeaveMessages", true);
        config.addDefault("buildingAllowed", true);
        config.addDefault("announceWeatherChanges", true);
        config.options().copyDefaults(true);
        saveConfig();
    }
    public String motdFormat(String line) {
        Server server = this.getServer();
        String formattedLine = line;
        // Strings
        formattedLine = formattedLine.replaceAll(":player_count:", server.getOnlinePlayers().size() + "/" + server.getMaxPlayers());
        formattedLine = formattedLine.replaceAll(":online_players:", String.format("%d", server.getOnlinePlayers().size()));
        formattedLine = formattedLine.replaceAll(":max_players:", String.format("%d", server.getMaxPlayers()));
        formattedLine = formattedLine.replaceAll(":server_version:", server.getVersion());
        formattedLine = formattedLine.replaceAll(":bukkit_version:", server.getBukkitVersion());
        formattedLine = formattedLine.replaceAll(":server_name:", server.getMotd());
        formattedLine = formattedLine.replaceAll(":amethyst_version:", "Amethyst " + this.version);
        // Colors
        formattedLine = formattedLine.replaceAll(":black:", "§0");
        formattedLine = formattedLine.replaceAll(":dark_blue:", "§1");
        formattedLine = formattedLine.replaceAll(":dark_green:", "§2");
        formattedLine = formattedLine.replaceAll(":dark_aqua:", "§3");
        formattedLine = formattedLine.replaceAll(":dark_red:", "§4");
        formattedLine = formattedLine.replaceAll(":dark_purple:", "§5");
        formattedLine = formattedLine.replaceAll(":gold:", "§6");
        formattedLine = formattedLine.replaceAll(":gray:", "§7");
        formattedLine = formattedLine.replaceAll(":dark_gray:", "§8");
        formattedLine = formattedLine.replaceAll(":blue:", "§9");
        formattedLine = formattedLine.replaceAll(":green:", "§a");
        formattedLine = formattedLine.replaceAll(":aqua:", "§b");
        formattedLine = formattedLine.replaceAll(":red:", "§c");
        formattedLine = formattedLine.replaceAll(":light_purple:", "§d");
        formattedLine = formattedLine.replaceAll(":yellow:", "§e");
        formattedLine = formattedLine.replaceAll(":white:", "§f");
        // Formats
        formattedLine = formattedLine.replaceAll(":obfuscated:", "§k");
        formattedLine = formattedLine.replaceAll(":bold:", "§l");
        formattedLine = formattedLine.replaceAll(":underline:", "§m");
        formattedLine = formattedLine.replaceAll(":strikethrough:", "§n");
        formattedLine = formattedLine.replaceAll(":italic:", "§o");
        formattedLine = formattedLine.replaceAll(":reset:", "§r");

        return formattedLine;
    }
}
