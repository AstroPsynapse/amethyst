package org.astropsynapse.Amethyst.resources;

import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public class StaticLists {
    public final List<String> Materials;

    public StaticLists() {
        Materials = new ArrayList<>();
        getMaterialsList();
    }

    private void getMaterialsList() {
        for (Material material : Material.values()) {
            Materials.add(material.name().toLowerCase());
        }
    }
}
