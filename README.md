# Amethyst #

### What is it? ###

Amethyst is a light-weight plugin written in Java for Bukkit/Spigot Minecraft servers. The overall goal is to simplify what Essentials/EssentialsX is into a smaller, more modular plugin.


### Commands / Permission Nodes ###

| Permission Node                  | Command           | Description                                                     |
| -------------------------------- | ----------------- | --------------------------------------------------------------- |
| `amethyst.admin`                 | `/amethyst`       | Controls various aspects of Amethyst.                           |
| `amethyst.teleport.spawn`        | `/spawn`          | Teleports the player to the world's spawn point.                |
| `amethyst.teleport.tp`           | `/tp`             | Teleports to the specified player.                              |
| `amethyst.teleport.tp.other`     | `/tp`             | Can also be used to teleport one player to another player.      |
| `amethyst.teleport.back`         | `/back`           | Returns the player back to their previous position.             |
| `amethyst.home.sethome`          | `/sethome`        | Sets your current location as a home.                           |
| `amethyst.home.delhome`          | `/delhome`        | Deletes the specified home from your list of saved homes.       |
| `amethyst.home.home`             | `/home`           | Teleports to the specified home location.                       |
| `amethyst.home.home`             | `/homes`          | Lists all of the player's saved homes.                          |
| `amethyst.moderation.item.other` | `/give`           | Gives the specified player the specified item.                  |
| `amethyst.moderation.item`       | `/item`           | Grants the player the specified item.                           |
| `amethyst.moderation.mute`       | `/mute`           | Mutes or unmutes the specified player.                          |
| `amethyst.moderation.setspawn`   | `/setspawn`       | Sets the spawn point to the player's current position.          |
| `amethyst.user.motd`             | `/motd`           | Shows the server's message of the day.                          |
| `amethyst.user.nick`             | `/nick`           | Changes the player's nickname, or resets it.                    |
| `amethyst.other.clearinventory`  | `/ci`             | Completely clears a player's inventory.                         |
| `amethyst.other.lightning`       | `/lightning`      | Casts a lightning strike where the player is currently looking. |
